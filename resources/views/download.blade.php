<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Download Page</title>

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 96px;
            }

            .button {
                padding: 50 px;
            }
        </style>
    </head>

</head>
<body>
    <div class="container">
        <div class="content">
            <div>
                <a href="download/ReportBPBD" download="ReportBPBD.xlsx">
                    <button type="button" class="btn btn-primary">
                        <i class="glyphicon glyphicon-download">
                            Download
                        </i>
                    </button>
                </a>
            </div>
        </div>
    </div>
</body>
</html>