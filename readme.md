# API BPBD Sentiment Analyst

[![Build Status](https://travis-ci.org/laravel/framework.svg)](https://travis-ci.org/laravel/framework)
[![License](https://poser.pugx.org/laravel/framework/license.svg)](https://packagist.org/packages/laravel/framework)

Monitoring any social media like twiiter, facebook, and news portal.

## Official Documentation

Ongoing.

## Installation 

- Required : php7.2 , composer, mysql.
- Clone this project. 
- Run `composer install`
- Run `php artisan migrate`
- Run `php storage:link`
- Run `ln -s /path/to/laravel/storage/app/public /path/to/public/storage` to link project storage to your local storage/server.
- Run `php artisan serve` to run in your local machine

## Contributing

- Dzulfan 
- Syamsul
- Haris

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell at taylor@laravel.com. All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
