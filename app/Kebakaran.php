<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kebakaran extends Model
{
    protected $table = 'kebakaran';

    public function getDateValue(){
        return date('d-m-Y',strtotime($this->dates));
    }
}
