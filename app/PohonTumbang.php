<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PohonTumbang extends Model
{
    protected $table = 'pohon_tumbang';

    public function getDateValue(){
        return date('d-m-Y',strtotime($this->dates));
    }
}
