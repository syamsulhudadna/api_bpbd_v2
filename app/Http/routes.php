<?php

Route::get('/', function () {
    return view('welcome');
});

Route::group([
    'prefix'=>'auth'
],function () {
    Route::post('/login', 'AuthController@login');
    Route::post('/signup', 'AuthController@signup');

    Route::group([
        'middleware' => 'auth:api'
    ], function() {
        Route::get('/logout', 'AuthController@logout');
        Route::get('/user', 'AuthController@user');
    });
});

Route::group([
    'prefix'=>'api',
    'middleware'=>'auth:api'    
],function(){
    Route::get('/banjir','BanjirController@index');
    Route::get('/kecelakaan','KecelakaanController@index');
    Route::get('/kebakaran','KebakaranController@index');
    Route::get('/pohon_tumbang','PohonTumbangController@index');
    Route::get('/count_banjir','BanjirController@count');
    Route::get('/count_kecelakaan','KecelakaanController@count');
    Route::get('/count_kebakaran','KebakaranController@count');
    Route::get('/count_pohon_tumbang','PohonTumbangController@count');
    Route::get('/reportDaily','ReportController@getReport');
    Route::get('/countAll','ReportController@getGraph');
    Route::get('/download','ReportController@exportExcel');
    ROute::get('/example/download','ReportController@exampleDownload');

});

    Route::get('/facebook','ReportController@getFacebook');
    Route::get('/crawl1','ReportController@crawl1');
    Route::get('/crawl2','ReportController@crawl2');
    
    Route::get('/getException','ExeptionController@getException');
    Route::post('/insertException','ExeptionController@insertException');