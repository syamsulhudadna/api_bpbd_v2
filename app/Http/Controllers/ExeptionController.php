<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;

use Carbon\Carbon;
use App;
use App\Http\Requests;
use App\Banjir;
use App\Kecelakaan;
use App\PohonTumbang;
use App\Kebakaran;
use App\Exception;
use Storage;

class ExeptionController extends Controller
{

    public function getException(Request $request){
    	$paginate = $request->query('paginate');
    	$exception = Exception::paginate($paginate);
    	$exception_count = Exception::all()->count();

        
        return [
            'status'=>200,
            'values'=>[
                'exception'=>$exception,
                'exception_count'=>$exception_count
            ]
        ];
    }

    public function insertException(Request $request){
    	$keyword = $request->input('keyword');

    	DB::table('exception')->insert(
		    ['name' => $keyword]
		);

        
        return [
            'status'=>200,
            'values'=>[
                'value'=> 'SUCCESS'
            ]
        ];
    }



}
