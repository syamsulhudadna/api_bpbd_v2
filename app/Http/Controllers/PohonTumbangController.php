<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\PohonTumbang;

class PohonTumbangController extends Controller
{
    protected function roundPages($total, $limit)
    {
        $page = round($total / $limit, 0);
        if ($total > ($limit * $page))
            $page = $page + 1;
        return intval($page);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = $request->query('limit',30);
        $offset = $request->query('offset',0);
        $sort = $request->query('sort','desc');
        $e = PohonTumbang::orderBy('id',$sort);
        $ndata = $e->count();
        $data = $e->limit($limit)
                ->offset($offset)
                ->get();
        $totalpages = $this->roundPages( $ndata,$limit);
        $value = [];
        $currentpage = 1;
        if ($offset>$limit){
            $currentpage = $this->roundPages( $offset,$limit);
        }

        $value = (object)[
            'data'=>$data,
            'pagination'=>[
                'current_page'=>$currentpage,
                'prev_page'=> $currentpage-1,
                'next_page'=> $currentpage+1,
                'total_page'=> $totalpages,
                'total_items'=> $ndata
            ]
        ];

        return [
            'status_code'=>200,
            'values'=>$value,
        ];
    }


    public function count(){
        $ndata = PohonTumbang::count();
        return [
            'status_code'=>200,
            'value'=>[
                'count'=>$ndata
            ]
        ];
    }
}
