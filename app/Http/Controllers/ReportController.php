<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use Carbon\Carbon;
use App;
use App\Http\Requests;
use App\Banjir;
use App\Kecelakaan;
use App\PohonTumbang;
use App\Kebakaran;
use App\Exception;
use Storage;

class ReportController extends Controller
{
    protected function roundPages($total, $limit)
    {
        $page = round($total / $limit, 0);
        if ($total > ($limit * $page))
            $page = $page + 1;
        return intval($page);
    }

    protected function percent($ndata,$total){
        if($total==0){
            return '25 %';
        }
        $result = ($ndata/$total)*100;
        return ''.floor($result).' %';
    }

    public function getReport(Request $request){
        $yDay = Carbon::now()->subDay(30);
        $tDay = Carbon::now();
        $i = 0;
        $out = [];
        $datas = [];
        $limit = $request->query('limit',30);
        $offset = $request->query('offset',0);
        if ($offset>0){
            $tDay->subDay($offset);
        }
        while($tDay > $yDay && $i<$limit){
            $key = $tDay;
            $nbanjir = Banjir::where('dates', 'like', '%'.$key->format('M d').'%'.$key->format('Y').'%')
                    ->count();
            $nkecelakaan = Kecelakaan::where('dates', 'like', '%'.$key->format('M d').'%'.$key->format('Y').'%')
                    ->count();
            $nkebakaran = Kebakaran::where('dates', 'like', '%'.$key->format('M d').'%'.$key->format('Y').'%')
                    ->count();
            $npohontumbang = PohonTumbang::where('dates', 'like', '%'.$key->format('M d').'%'.$key->format('Y').'%')
                    ->count();
            $data = (object)[
                'date'=>$key->format('d - m - Y'),
                'banjir'=>$nbanjir,
                'kecelakaan'=>$nkecelakaan,
                'kebakaran'=>$nkebakaran,
                'pohon_tumbang'=>$npohontumbang
            ];
            array_push($datas,$data);
            $tDay = $tDay->subDay();
            $i++;
        }
        $ndata=count($datas);
        $totalpages = $this->roundPages( $ndata,$limit);
        $currentpage = 1;
        if ($offset>$limit){
            $currentpage = $this->roundPages( $offset,$limit);
        }
        
        return [
            'status'=>200,
            'values'=>[
                'data'=>$datas,
                'pagination'=>[
                    'current_page'=>$currentpage,
                    'prev_page'=> $currentpage-1,
                    'next_page'=> $currentpage+1,
                    'total_page'=> $totalpages,
                    'total_items'=> $ndata
                ]
            ]
        ];
    }

    public function getGraph(Request $request){
        $yDay = Carbon::now()->subDay(7);
        $tDay = Carbon::now();
        $from = $request->query('from',$yDay->format('Y-m-d'));
        $to   = $request->query('to',$tDay->format('Y-m-d'));
        $tos = strtotime($to);
        $froms = strtotime($from);
        $newto = date('Y-m-d',$tos);
        $newfrom = date('Y-m-d',$froms);

        $diff = strtotime($to) - strtotime($from);
        $days_between = ceil(abs($diff) / 86400);
        
        $sumA = 0;
        $sumB = 0;
        $sumC = 0;
        $sumD = 0; 

        $i = 0;
        while($days_between >= 0){
            $key = $froms;
            $ndbanjir = Banjir::where('dates', 'like', '%'.date('M d',$key).'%'.date('Y',$key).'%')
                    ->count();
            $ndkecelakaan = Kecelakaan::where('dates', 'like', '%'.date('M d',$key).'%'.date('Y',$key).'%')
                    ->count();
            $ndkebakaran = Kebakaran::where('dates', 'like', '%'.date('M d',$key).'%'.date('Y',$key).'%')
                    ->count();
            $ndpohontumbang = PohonTumbang::where('dates', 'like', '%'.date('M d',$key).'%'.date('Y',$key).'%')
                    ->count();
            $froms = $froms+86400;
            $sumA+=$ndbanjir;
            $sumB+=$ndkecelakaan;
            $sumC+=$ndkebakaran;
            $sumD+=$ndpohontumbang;
            $i++;
            $days_between--;
        }
        $sum = $sumA + $sumB + $sumC + $sumD;
        return [
            'status'=>200,
            'values'=>[
                'kebakaran'=>[
                    'total'=>$sumC,
                    'persentase'=>$this->percent($sumC,$sum),
                ],
                'banjir'=>[
                    'total'=>$sumA,
                    'persentase'=>$this->percent($sumA,$sum),
                ],
                'kecelakaan'=>[
                    'total'=>$sumB,
                    'persentase'=>$this->percent($sumB,$sum),
                ],
                'pohon_tumbang'=>[
                    'total'=>$sumD,
                    'persentase'=>$this->percent($sumD,$sum),
                ]
            ]
        ];
    }


    public function getFacebook(Request $request){
        $q = $request->query('q');
        $center = $request->query('center');
        $access_token = $request->query('access_token');

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, "https://graph.facebook.com/v3.2/search?pretty=0&q=banjir&type=place&center=-63.475%2C106.785&limit=25&after=MjQZD&access_token=EAAhOMGYqZC0EBAO1SFUcE3pc4m9P7fvMOr53M1vZB9DvBi7WtJXGDvRSNQ7v9WfJR7JrTryPrDBfmPjUh0mPEXcNBMtJ7fsaNjfeQhvZCZAKaVvQDf8ppPlNzmOiRzXxFZBolNIyZCDizYc5AavXBXW6TWEzXOkQashqH3T0RaUS6Q6F8CPrMU1o3Av1G7B3yi4Uc8g9pKbQZDZD");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $result = curl_exec($ch);
        curl_close ($ch);


        return json_decode($result,true);
    }


    public function crawl1(Request $request){
        $keyword = $request->query('keyword');

        $import = "from tweepy.streaming import StreamListener".PHP_EOL."from tweepy import OAuthHandler".PHP_EOL."from tweepy.streaming import StreamListener".PHP_EOL."from tweepy import OAuthHandler".PHP_EOL."from tweepy import Stream"."import json".PHP_EOL."import tweepy".PHP_EOL."import datetime".PHP_EOL."import sys".PHP_EOL."import MySQLdb".PHP_EOL."import mysql.connector".PHP_EOL.PHP_EOL;

        $ondata = "class StdOutListener(StreamListener):".PHP_EOL."\t def on_data(self, data):".PHP_EOL."\t \t fhOut.write(data)".PHP_EOL."\t \t j=json.loads(data)".PHP_EOL.PHP_EOL."\t \t mydb = mysql.connector.connect(".PHP_EOL."\t \t \t host='localhost',".PHP_EOL."\t \t \t user='root',".PHP_EOL."\t \t \t passwd='mticeria',".PHP_EOL."\t \t \t database='datas',".PHP_EOL."\t \t \t charset='utf8mb4'".PHP_EOL."\t \t )".PHP_EOL."\t \t mycursor = mydb.cursor()".PHP_EOL."\t \t sql = 'INSERT INTO kebakaran (dates, texts, subject) VALUES (%s, %s, %s)'".PHP_EOL."\t \t val = (j['created_at'],j['text'],j['user']['name'])".PHP_EOL."\t \t mycursor.execute(sql, val)".PHP_EOL.PHP_EOL."\t \tmydb.commit()".PHP_EOL;

        $error = "\t def on_error(self, status):".PHP_EOL."\t \t print('ERROR')".PHP_EOL."\t \t print(status)".PHP_EOL;


        $condition = "if __name__ == '__main__':".PHP_EOL."\t try:".PHP_EOL."\t \t fhOut = open('output.json','a')".PHP_EOL.PHP_EOL."\t \t consumer_key = 'lJK35VKAOXZwGpclm8dDZ8PpS'".PHP_EOL."\t \t consumer_secret = 'Wf3hPgpG9dhj2e6m6ZzHkValhJ0iV5C3iRoL9zt8wygDMqecfq'".PHP_EOL."\t \t access_token = '96937017-kYtrOoD6nJRZ1MOBNurnkUPzj6t7KNXxmQ5Tk6IV7'".PHP_EOL."\t \t access_token_secret = 'T6hPuQFsv70VlCQYjpv260fQ7dynTNMzkKAw5ACypBC6A'".PHP_EOL.PHP_EOL."\t \t l = StdOutListener()".PHP_EOL."\t \t auth = OAuthHandler(consumer_key, consumer_secret)".PHP_EOL."\t \t auth.set_access_token(access_token, access_token_secret)".PHP_EOL.PHP_EOL."\t \t stream = Stream(auth, l)".PHP_EOL."\t \t stream.filter(track=['".$keyword."'])".PHP_EOL."\t \t stream.filter(locations=[106.314674,-6.374457,106.973976,-6.374457])".PHP_EOL."\t except KeyboardInterrupt:".PHP_EOL."\t \t pass".PHP_EOL."\t fhOut.close()";

        $data = $import.$ondata.$error.$condition;


        $fp = fopen('../storage/app/test.py','w');
        fwrite($fp, $data);
        fclose($fp);

        return 1;
    }


    public function crawl2(Request $request){
        $keyword = $request->query('keyword');

        $import = "from tweepy.streaming import StreamListener".PHP_EOL."from tweepy import OAuthHandler".PHP_EOL."from tweepy.streaming import StreamListener".PHP_EOL."from tweepy import OAuthHandler".PHP_EOL."from tweepy import Stream"."import json".PHP_EOL."import tweepy".PHP_EOL."import datetime".PHP_EOL."import sys".PHP_EOL."import MySQLdb".PHP_EOL."import mysql.connector".PHP_EOL.PHP_EOL;

        $ondata = "class StdOutListener(StreamListener):".PHP_EOL."\t def on_data(self, data):".PHP_EOL."\t \t fhOut.write(data)".PHP_EOL."\t \t j=json.loads(data)".PHP_EOL.PHP_EOL."\t \t mydb = mysql.connector.connect(".PHP_EOL."\t \t \t host='localhost',".PHP_EOL."\t \t \t user='root',".PHP_EOL."\t \t \t passwd='mticeria',".PHP_EOL."\t \t \t database='datas',".PHP_EOL."\t \t \t charset='utf8mb4'".PHP_EOL."\t \t )".PHP_EOL."\t \t mycursor = mydb.cursor()".PHP_EOL."\t \t sql = 'INSERT INTO kebakaran (dates, texts, subject) VALUES (%s, %s, %s)'".PHP_EOL."\t \t val = (j['created_at'],j['text'],j['user']['name'])".PHP_EOL."\t \t mycursor.execute(sql, val)".PHP_EOL.PHP_EOL."\t \tmydb.commit()".PHP_EOL;

        $error = "\t def on_error(self, status):".PHP_EOL."\t \t print('ERROR')".PHP_EOL."\t \t print(status)".PHP_EOL;


        $condition = "if __name__ == '__main__':".PHP_EOL."\t try:".PHP_EOL."\t \t fhOut = open('output.json','a')".PHP_EOL.PHP_EOL."\t \t consumer_key = 'lJK35VKAOXZwGpclm8dDZ8PpS'".PHP_EOL."\t \t consumer_secret = 'Wf3hPgpG9dhj2e6m6ZzHkValhJ0iV5C3iRoL9zt8wygDMqecfq'".PHP_EOL."\t \t access_token = '96937017-kYtrOoD6nJRZ1MOBNurnkUPzj6t7KNXxmQ5Tk6IV7'".PHP_EOL."\t \t access_token_secret = 'T6hPuQFsv70VlCQYjpv260fQ7dynTNMzkKAw5ACypBC6A'".PHP_EOL.PHP_EOL."\t \t l = StdOutListener()".PHP_EOL."\t \t auth = OAuthHandler(consumer_key, consumer_secret)".PHP_EOL."\t \t auth.set_access_token(access_token, access_token_secret)".PHP_EOL.PHP_EOL."\t \t stream = Stream(auth, l)".PHP_EOL."\t \t stream.filter(track=['".$keyword."'])".PHP_EOL."\t \t stream.filter(locations=[106.314674,-6.374457,106.973976,-6.374457])".PHP_EOL."\t except KeyboardInterrupt:".PHP_EOL."\t \t pass".PHP_EOL."\t fhOut.close()";

        $data = $import.$ondata.$error.$condition;


        $fp = fopen('../storage/app/test.py','w');
        fwrite($fp, $data);
        fclose($fp);

        return 1;
    }



    public function exportExcel(){
        $excel = App::make('excel');
        $filename = 'BPBDReports';
        $x = $excel->create($filename, function($excel) {
            $excel->setTitle('Report Data Bencana');
            $excel->setCreator('BPBD Admin')->setCompany('BPBD');
            $excel->setDescription('report data dari database');
            $excel->sheet('Banjir', function($sheet) {
                $data = Banjir::all();
                $sheet->fromModel($data);
            });
            $excel->sheet('Kebakaran', function($sheet) {
                $data = Kebakaran::all();
                $sheet->fromModel($data);
            });
            $excel->sheet('Kecelakaan', function($sheet) {
                $data = Kecelakaan::all();
                $sheet->fromModel($data);
            });
            $excel->sheet('Pohon Tumbang', function($sheet) {
                $data = PohonTumbang::all();
                $sheet->fromModel($data);
            });
        })->store('xlsx',storage_path('app/public/download'));

        return [
            'status_code'=>200,
            'message'=>'download report file success',
            'value'=>[
                'file_name'=>$filename.'.xlsx',
            ]
        ];
    }

    public function exampleDownload(){
        return view('download');
    }

    public function delete_data(){
        Banjir::where('id_kecamatan', 100)->delete();
        Banjir::where('id_kecamatan', 100)->delete();
        PohonTumbang::where('id_kecamatan', 100)->delete();
        Kecelakaan::where('id_kecamatan', 100)->delete();

        return 1;
    }
}
