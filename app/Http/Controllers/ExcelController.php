<?php

namespace App\Http\Controllers;

use App\Exports\BanjirExport;
use App\Exports\KecelakaanExport;
use App\Exports\KebakaranExport;
use App\Exports\PohonTumbangExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;

class ExcelController extends Controller 
{
    public function exportBanjir() 
    {
        return Excel::download(new BanjirExport, 'banjir.xlsx');
    }
    public function exportKebakaran() 
    {
        return Excel::download(new KebakaranExport, 'kebakaran.xlsx');
    }
    public function exportKecelakaan() 
    {
        return Excel::download(new KecelakaanExport, 'kecelakaan.xlsx');
    }
    public function exportPohonTumbang() 
    {
        return Excel::download(new PohonTumbangExport, 'pohon_tumbang.xlsx');
    }
}