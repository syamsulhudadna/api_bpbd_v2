<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kecelakaan extends Model
{
    protected $table = 'kecelakaan';
    
    public function getDateValue(){
        return date('d-m-Y',strtotime($this->dates));
    }

}
