<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Exception extends Model
{
    protected $table = 'exception';

    public function getDateValue(){
        return date('d-m-Y',strtotime($this->dates));
    }
}
